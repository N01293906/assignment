﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HotelBooking.aspx.cs" Inherits="Assignment_1.HotelBooking" %>

<script runat="server">
    void HotelBooking(Object sender, EventArgs e)
    {
        Button clickedbutton = (Button)sender;
        clickedbutton.Text = "Checking Entry";
        clickedbutton.Enabled = false;

    }
</script>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Stay Here</title>
    <link rel="icon" href="icon.png" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>Room Booking</h1>
            <asp:Label runat="server" Text="Title" AssociatedControlID="title"></asp:Label>
            <asp:DropDownList runat="server" ID="title">
                <asp:ListItem Text="Mr." Value="Mr."></asp:ListItem>
                <asp:ListItem Text="Ms." Value="Ms."></asp:ListItem>
                <asp:ListItem Text="Mrs." Value="Mrs."></asp:ListItem>
            </asp:DropDownList>

            <asp:Label runat="server" Text="First Name" AssociatedControlID="clientFirstName" ID="clientFirstNameLabel"></asp:Label> 
            <asp:TextBox runat="server" ID="clientFirstName" placeholder="First Name" CausesValidation="true" ></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Enter First Name!!" ControlToValidate="clientFirstName" ID="validatorFirstName"></asp:RequiredFieldValidator>
            
            <asp:Label runat="server" Text="Last Name" ID="clientLastNameLabel" AssociatedControlID="clientLastName"></asp:Label>
            <asp:TextBox runat="server" ID="clientLastName" placeholder="Last Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter Last Name!!" ControlToValidate="clientLastName" ID="validatorLastName"></asp:RequiredFieldValidator>
        </div>
        <div>
            <asp:Label runat="server" Text="Email" ID="clientEmaillabel" AssociatedControlID="clientEmail"></asp:Label>
            <asp:TextBox runat="server" ID="clientEmail" placeholder="Email" ></asp:TextBox>
            <asp:RegularExpressionValidator runat="server" ID="validatorEmail" ControlToValidate="clientEmail" ErrorMessage="Invalid Email format!!!!" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            <asp:RequiredFieldValidator ID="validatorFieldEmail"  runat="server" ControlToValidate="clientEmail" ErrorMessage="Please Enter Email.."></asp:RequiredFieldValidator>
        </div>
        <div>
             <asp:Label runat="server" Text="Confirm Email" ID="confirmClientEmailLabel" AssociatedControlID="confirmClientEmail"></asp:Label>
            <asp:TextBox runat="server" ID="confirmClientEmail" placeholder="Email"></asp:TextBox>
            <asp:CompareValidator runat="server" ID="compareEmail" ErrorMessage="Email do no match" ControlToCompare="clientEmail" ControlToValidate="confirmClientEmail"></asp:CompareValidator>
            <asp:RegularExpressionValidator runat="server" ID="validatorConfirmClientEmail" ControlToValidate="confirmClientEmail" ErrorMessage="Invalid Email format!!!!" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
            <asp:RequiredFieldValidator ID="validatorConfirmFieldEmail"  runat="server" ControlToValidate="confirmClientEmail" ErrorMessage="Please Enter Email.."></asp:RequiredFieldValidator>
        </div>
        <div>
            <asp:Label ID="foodFacilityLabel" Text="Food Facility" runat="server"></asp:Label>
             <div id="foodFacility" runat="server">
            <asp:CheckBox runat="server" ID="breakfast" Text="Breakfast" />
            <asp:CheckBox runat="server" ID="lunch" Text="Lunch" />
            <asp:CheckBox runat="server" ID="dinner" Text="Dinner" />
             </div>
        </div>
        <div>
            <asp:Label runat="server" ID="paymentModeLabel" Text="Mode of Payment" AssociatedControlID="paymentMode"></asp:Label>
            <asp:RadioButtonList runat="server" ID="paymentMode">
                <asp:ListItem runat="server" Text="Debit card" GroupName="paymentMode" />
                <asp:ListItem runat="server" Text="Online banking" GroupName="paymentMode"/>
            </asp:RadioButtonList>
            <asp:RequiredFieldValidator runat="server" ID="validatorfielPaymentMode" ControlToValidate="paymentMode" ErrorMessage="Please select Payment mode"></asp:RequiredFieldValidator>
        </div>
        <div>
            <asp:Button Text="Submit" runat="server" OnClick="HotelBooking" ID="submitButton" />
        </div>
    </form>
</body>
</html>
